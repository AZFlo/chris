$(document).ready(function() {

    // Initiales Bild setzen
    $(".menu_item").each(function(element)
    {
        var category = $(this).attr("data-category");
        $(this).css("background-image","url('./icons/" + category + ".svg')");
    });

    // Bei Mousein und Mouseout Klassen und Bilder ändern
    $(".menu_item").hover(
        function()
        {
            var category = $(this).attr("data-category");
            $(this).addClass("active");
            $(this).css("background-image","url('./icons/" + category + "_aktiv.svg')");
        }, 
        function()
        {
            var category = $(this).attr("data-category");
            $(this).removeClass("active");

            if(!$(this).hasClass("selected"))
            {
                $(this).css("background-image","url('./icons/" + category + ".svg')");
            }
        }
    );

    $(".menu_item").click(function()
    {
        var category = $(this).attr("data-category");

        $(".menu_item").each(function(element)
        {
            $(this).removeClass("selected");

            var cat = $(this).attr("data-category");
            $(this).css("background-image","url('./icons/" + cat + ".svg')");
        });

        // Selected Klasse zuweisen
        $(this).addClass("selected");

        // Bild ändern
        $(this).css("background-image","url('./icons/" + category + "_aktiv.svg')");

        // Lade Produkt Kategorie
        loadCategory(category);
    });

});


function loadCategory(category)
{
    $("#product_category").html("Produkte der Kategorie: " + category);
    $("#product_view").html("Keine Produkte verfügbar!");
}


